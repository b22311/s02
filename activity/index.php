<?php require_once "./code.php";?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Activity s02</title>
</head>
<body>
   <h2>1.</h2>
   <?php isDivisibleByFive();?>

   <h2>2.</h2>
   <h4>a.</h4>
   <?php array_push($students, "Max");?>
   <pre><?php print_r($students);?></pre>

   <h4>b.</h4>
   <pre><?php echo count($students);?></pre>

   <h4>c.</h4>
   <?php array_push($students, "Sky");?>
   <pre><?php print_r($students);?></pre>  

   <h4>d.</h4>
   <pre><?php echo count($students);?></pre>

   <h4>e.</h4>
   <?php array_pop($students);?>
   <pre><?php print_r($students);?></pre>

   <h4>f.</h4>
   <pre><?php echo count($students);?></pre>


</body>
</html>