<?php require_once "./code.php";?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S02 Repetition Control Structures and Array Manipulation</title>
</head>
<body>
    <h1>Repetition Control Structures</h1>

    <h2>While Loop</h2>
    <?php whileLoop()?>

    <h2>Do-While Loop</h2>
    <?php doWhileLoop()?>
    
    <h2>For Loop</h2>
    <?php forLoop()?>

    <h2>Continue and Break</h2>
    <?php modifiedForLoop()?>
    
    <h1>Array Manipulation</h1>
    <h2>types of array</h2>

    <h3>Simple Array</h3>
    <!-- syntax:
         foreach($array as $value/element){
            code to be executed
         } -->
    <ul>
        <?php foreach($computerBrands as $brand) { ?>
            <li><?= $brand; ?></li>
        <?php } ?>
    </ul>

    <h3>Associative Array</h3>
    <ul>
        <?php foreach($gradePeriods as $period => $grade) { ?>
            <li>
                Grade in <?= $period;?> is <?= $grade; ?>
            </li>
        <?php } ?> 
    </ul>

    <h3>Multidimentional Arrays</h3>
    <!-- <pre><?php print_r($heroes); ?></pre> -->
    
    <?php
        foreach($heroes as $team){
            foreach($team as $member){
    ?>
    <li>
        <?= $member; ?>
    </li>
    <?php
            }
        }
    ?>

    <h3>Multi-Dimentional associative array</h3>
    <ul>
        <?php
            foreach($ironManPowers as $label => $powerGroup){
                foreach($powerGroup as $power){
        ?>
            <li>
                <?= "$label: $power"; ?>
            </li>
        <?php
                }
            }
        ?>
    </ul>

    <h2>Array Functions</h2>
    <h3>Original Array</h3>
    <pre><?php print_r($computerBrands);?></pre>

    <h3>Sorting</h3>
    <pre><?php print_r($sortedBrands);?></pre>

    <h3>Sorting (reverse)</h3>
    <pre><?php print_r($reverseSortedBrands);?></pre>

    <h3>Append</h3>
        <?php array_push($computerBrands, "apple");?>
        <pre><?php print_r($computerBrands);?></pre>

        <?php array_unshift($computerBrands, "dell");?>
        <pre><?php print_r($computerBrands);?></pre>

    <h3>Remove</h3>
        <?php array_pop($computerBrands);?>
        <pre><?php print_r($computerBrands);?></pre>

        <?php array_shift($computerBrands);?>
        <pre><?php print_r($computerBrands);?></pre>
    
    <h3>Others</h3>
    <h4>Count</h4>
            <pre><?php echo count($computerBrands);?></pre>
            

    <h4>In array</h4>
    <p><?php echo searchBrand($computerBrands, "acer");?></p>

</body>
</html>