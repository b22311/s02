<?php

// [Section] Repetition Control Structures

//while loop

function whileLoop(){
    $count = 5;

    while($count !== 0){
        echo $count . '<br/>';
        $count--;
    }
}

// Do-While Loop

function doWhileLoop(){
    $count =20;

    do {
        echo $count . '<br/>';
        $count--;
    } while($count > 0);
}

// for loop

function forLoop(){
    for($count = 0; $count <= 10; $count++){
        echo $count . '<br/>';
    }
}

// continue and break
function modifiedForLoop(){
    for($count = 0; $count <= 20; $count++){
        if($count % 2 === 0){
            continue;
        } 
        echo $count . '<br/>';
        if($count > 10){
            break;
        }
    }
}

// [Section] array manipulation

//$studentNumbers = array('2020-1923', '2020-1924','2020-1925','2020-1926','2020-1927');

//new php update - after PHP 5.4
$studentNumbers = ['2020-1923', '2020-1924','2020-1925','2020-1926','2020-1927'];

// simple array
$grade = [98.4,94.4,89.3,90.3];
$computerBrands = ["acer", "asus", "lenovo", "neo", "toshiba", "Mac"];

// Associative arrray
$gradePeriods = [
    'firstGrading'  => 98.5,
    'secondGrading' => 94.3,
    'thirdGrading'  => 90.1,
    'fourthGrading' => 89.1
];

// multidimentional array(two dimention)
$heroes = [
    ["ironman", "thor", "hulk"],
    ["wolverine", "cyclops", "jean gray"],
    ["batman","superman","wonder woman"]
];

// two-dimentional associative array
$ironManPowers = [
    "regular" => ["repulsor blast", "rocket Punch"],
    "signature" => ["unibeam"]
];

// array sorting
$sortedBrands = $computerBrands;
$reverseSortedBrands = $computerBrands;

// ascending order
sort($sortedBrands);
// descending
rsort($reverseSortedBrands);

// search function
function searchBrand($brands, $brand){
    // in_array($searchVal, $arrayList)
    return(in_array($brand, $brands) ? "$brand is in the array" : "$brand not in array");
}



